/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab01;

/**
 *
 * @author Admin
 */
import java.util.Scanner;
public class Lab01 {
    private char[][] board;
    private char Player;

    public Lab01() {
        board = new char[3][3];
        Player = 'X';
        xoBoard();
    }
    public void xoBoard() {
    int counter = 1;
    for (int row = 0; row < 3; row++) {
        for (int col = 0; col < 3; col++) {
            board[row][col] = Character.forDigit(counter, 10);
            counter++;
        }
    }
}
    public void printBoard() {
    for (int row = 0; row < 3; row++) {
        for (int col = 0; col < 3; col++) {
            System.out.print(board[row][col]);
            if (col < 2) {
                System.out.print(" | ");
            }
        }
        System.out.println();
        if (row < 2) {
            System.out.println("---------");
        }
    }
}
    
    public boolean Mark(int position) {
        if (position >= 1 && position <= 9) {
            int row = (position - 1) / 3;
            int col = (position - 1) % 3;
            if (board[row][col] != 'X' && board[row][col] != 'O') {
                board[row][col] = Player;
                return true;
            }
        }
        return false;
    }
        public void changePlayer() {
        Player = (Player == 'X') ? 'O' : 'X';
    }
    
    public boolean Winner(){
        return checkRow() || checkCol() || checkCross();
    }
    private boolean checkRow(){
        for (int i = 0; i < 3; i++){
            if (board[i][0] != '-' && board[i][0] == board[i][1] && board[i][0] == board[i][2]){
                return true;
            }
        }
        return false;
    }
    private boolean checkCol(){
        for (int j = 0; j < 3; j++){
            if (board[0][j] != '-' && board[0][j] == board[1][j] && board[0][j] == board[2][j]){
                return true;
            }
        }
        return false;
    }
    private boolean checkCross(){
            if (board[0][0] == board[1][1] && board[0][0] == board[0][1]){
                return true;
            }
            if (board[0][2] == board[1][1] && board[0][2] == board[2][0]){
                return true;
            }
            return false;
    }
        public boolean isBoardFull() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (board[row][col] != 'X' && board[row][col] != 'O') {
                    return false;
                }
            }
        }
        return true;
    }

        public static void main(String[] args) {
            Scanner kb = new Scanner(System.in);
            Lab01 game = new Lab01();
            boolean gameOver = false;
            Lab01 gameBoard = new Lab01();
            int position;

            System.out.println("xoGame");
            System.out.println("Player " + game.Player + ", enter position: ");
                
 while (!gameOver) {
            game.printBoard();
            do {
                System.out.print("Player " + game.Player + ", enter position: ");
                position = kb.nextInt();
            } while (!game.Mark(position));

            if (game.Winner()) {
                game.printBoard();
                System.out.println("Player " + game.Player + " wins!");
                gameOver = true;
            } else if (game.isBoardFull()) {
                game.printBoard();
                System.out.println("It's a tie!");
                gameOver = true;
            } else {
                game.changePlayer();
            }
        }

        kb.close();
    }
}
